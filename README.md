# Scheduling - xv6 :

Implement any 4 scheduling algorithms in xv6, e.g. Lottery, Priority, Shortest job first, Multilevel feedback queue, etc, assuring whenever required different timer value for every process. Write testing code in usertests.c or otherwise if required, to test your code. The test code should do a statistical analysis of the throughput, cpu utilisation, and turn-around time. The test code should include a proper mix of processes to test each algorithm on it's strengths (e.g. I/O bound vs CPU bound processes, different priority processes, etc)

Each algorithm implementation (total up to 28): lottery: 7 marks, priority: 6 marks, shortest job first (with some approximations to estimate job time): 9 marks, multilevel queues: 10 marks, multilevel feedback queues: 15 marks. Any additional algorithm may be discussed with the evaluator.

Implementing additional kernel code to get scheduling statistics: 7 marks

Writing user-land application code to test scheduling algorithms: 15 marks

Reference:
https://github.com/remzi-arpacidusseau/ostep-projects/tree/master/scheduling-xv6-lottery
